FROM ubuntu:19.10
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update -y && apt upgrade -y
RUN apt install -y bc bison build-essential ccache curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libssl-dev libwxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip zlib1g-dev python libtinfo5 libncurses5
RUN apt install -y vim-common

RUN git config --global user.email "fake@name.com" && git config --global user.name Fake Name

RUN apt install -y wget curl
RUN wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip && unzip platform-tools-latest-linux.zip -d ~

RUN wget https://download.java.net/java/GA/jdk9/9.0.4/binaries/openjdk-9.0.4_linux-x64_bin.tar.gz
RUN tar -C /root/ -zxvf openjdk-9.0.4_linux-x64_bin.tar.gz

RUN mkdir -p /root/bin
RUN mkdir -p /root/android/lineage

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /root/bin/repo
RUN chmod a+x /root/bin/repo

ENV PATH="/root/platform-tools:/root/jdk-9.0.4/bin:/root/bin:${PATH}"

WORKDIR /root
ADD ./scripts/build.sh /root/build.sh
ADD ./scripts/startupScript.sh /root/startupScript.sh
ADD ./scripts/update-sources.sh /root/update-sources.sh
ADD ./scripts/commit-patches.sh /root/commit-patches.sh
RUN chmod a+x ./*.sh

WORKDIR /root/android/lineage

ENTRYPOINT ["/root/startupScript.sh"]
