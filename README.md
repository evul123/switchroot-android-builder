# Dockerized environment to build Switchroot Android image

## How to use

- Install docker
- Clone/Download this repo
- Either prepend `sudo` to every command after this step, or allow the current user to run `docker` without sudo
- Run `./build.sh`

## How to fetch the latest code changes and rebuild
- run `./build-latest.sh`
