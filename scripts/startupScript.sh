#!/bin/bash
_PATH=''
CONTAINER_ALREADY_STARTED="CONTAINER_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e $CONTAINER_ALREADY_STARTED ]; then
    touch $CONTAINER_ALREADY_STARTED
    echo "-- First container startup --"

    _PATH='/root/android/lineage'
    cd ${_PATH}
    repo init --depth=1  -u https://github.com/LineageOS/android.git -b lineage-16.0

    repo sync --no-clone-bundle --no-tags

    _PATH='/root/android/lineage/.repo'
    cd ${_PATH}
    git clone https://gitlab.com/switchroot/android/manifest.git local_manifests
    repo sync --no-clone-bundle --no-tags

    _PATH='/root/android/lineage/'
    cd ${_PATH}
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t nvidia-enhancements-p
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t nvidia-shieldtech-p
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t nvidia-beyonder-p
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t nvidia-nvgpu-p
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t joycon-p
    /root/android/lineage/vendor/lineage/build/tools/repopick.py -t icosa-bt


    _PATH='/root/android/lineage/frameworks/base'
    cd ${_PATH}
    patch -p1 < ../../.repo/local_manifests/patches/frameworks_base-rsmouse.patch

    _PATH='/root/android/lineage/frameworks/base'
    cd ${_PATH}
    patch -p1 < ../../.repo/local_manifests/patches/frameworks_base-beyonder-bootcomplete.patch

    _PATH='/root/android/lineage/device/nvidia/foster_tab'
    cd ${_PATH}
    patch -p1 < ../../.repo/local_manifests/patches/device_nvidia_foster_tab-beyonder.patch
else
    echo "-- Not first container startup --"
fi

exec "$@"

