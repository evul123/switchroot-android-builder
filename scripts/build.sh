#!/bin/bash

mkdir -p /root/android/lineage/out/target/product/foster_tab/vendor/lib/modules

sed -i '/BOARD_KERNEL_IMAGE_NAME := zImage/a BOARD_MKBOOTIMG_ARGS    += --cmdline " "' ~/android/lineage/device/nvidia/foster/BoardConfig.mk

_PATH='/root/android/lineage/'
cd ${_PATH}
source build/envsetup.sh
export USE_CCACHE=1
ccache -M 50G
#lunch lineage_foster_tab-userdebug
lunch lineage_foster_tab-eng

make bootimage -j$(nproc) && make vendorimage -j$(nproc) && make systemimage -j$(nproc)

cp /root/android/lineage/out/target/product/foster_tab/boot.img /tmp/boot.img
cp /root/android/lineage/out/target/product/foster_tab/vendor.img /tmp/vendor.img
cp /root/android/lineage/out/target/product/foster_tab/system.img /tmp/system.img
cp /root/android/lineage/out/target/product/foster_tab/obj/KERNEL_OBJ/arch/arm64/boot/dts/tegra210-icosa.dtb /tmp/tegra210-icosa.dtb
